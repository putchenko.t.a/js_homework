class hamburger{
    constructor(size,stuffing){
        this.size=size
        this.stuffing=stuffing
    }
}
function Hamburger(size, stuffing){}
    Hamburger.SIZE_SMALL={
        size:'small',
        callories:20,
        price:50
    }
    Hamburger.SIZE_LARGE ={
        callories:40,
        price:100
    }
    Hamburger.STUFFING_CHEESE = {
        callories:20,
        price:10
    }
    Hamburger.STUFFING_SALAD = {
        callories:5,
        price:20
    }
    Hamburger.STUFFING_POTATO = {
        callories:10,
        price:20
    }
    Hamburger.TOPPING_MAYO = {
        callories:5,
        price:20
    }
    Hamburger.TOPPING_SPICE = {
        callories0,
        price:15
    }


//Hamburger.prototype.addTopping = function (topping){}
//Hamburger.prototype.removeTopping = function (topping){}
//Hamburger.prototype.getToppings = function () ...
         
         /**
          * Узнать размер гамбургера
          */
         //Hamburger.prototype.getSize = function () ...
         
         /**
          * Узнать начинку гамбургера
          */
         //Hamburger.prototype.getStuffing = function () ...
         
         /**
          * Узнать цену гамбургера
          * return {Number} Цена в тугриках
          */
         //Hamburger.prototype.calculatePrice = function () ...
         
         /**
          * Узнать калорийность
          * return {Number} Калорийность в калориях
          */
         //Hamburger.prototype.calculateCalories = function () ...
         
         /**
          * Представляет информацию об ошибке в ходе работы с гамбургером. 
          * Подробности хранятся в свойстве message.
          * constructor 
          */
         //function HamburgerException (...) { ... }
         /*```
         ```javascript
         // маленький гамбургер с начинкой из сыра
         var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
         // добавка из майонеза
         hamburger.addTopping(Hamburger.TOPPING_MAYO);
         // спросим сколько там калорий
         console.log("Calories: %f", hamburger.calculateCalories());
         // сколько стоит
         console.log("Price: %f", hamburger.calculatePrice());
         // я тут передумал и решил добавить еще приправу
         hamburger.addTopping(Hamburger.TOPPING_SPICE);
         // А сколько теперь стоит? 
         console.log("Price with sauce: %f", hamburger.calculatePrice());
         // Проверить, большой ли гамбургер? 
         console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
         // Убрать добавку
         hamburger.removeTopping(Hamburger.TOPPING_SPICE);
         console.log("Have %d toppings", hamburger.getToppings().length); // 1
         ```
*/