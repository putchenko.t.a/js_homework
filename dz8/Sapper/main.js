//функция для создания матрицы
let getMatrix=(columns,rows)=>{
    const matrix=[];
    //счетчик для айди
    idCounter=1;
    for (let y=0;y<rows;y++){ 
        //создается массив рядков(8 штук)
        const row=[];
        for( let x=0;x<columns;x++){
            //в каждом рядке создается 8 объектов с заданными свойствами
            row.push({
                id: idCounter++,
                left:false,
                right:false,
                show:false,
                poten:false,
                x,
                y,
                flag:false,
                mine:false,
                number:0,
                win:false
            });
        }
        //всё это вставляется в саму матрицу
        matrix.push(row);
    }
    return matrix;
};
//функция для получения случайной свободной клетки
function getRandomFreeCell(matrix){
    //массив для клеток
    const freeCells=[];
    //пробегаюсь по матрице
    for (let y=0; y<matrix.length;y++){
        for (let x=0;x<matrix[y].length;x++){ 
            //1 клетка
            const cell=matrix[y][x];
            //если в ней нет мины - клетка добавляется в массив пустых
            if (!cell.mine){
              freeCells.push(cell);  
            }
        
        }
    }
    const index=Math.floor(Math.random()*freeCells.length);
    return freeCells[index];
};
//функция для установки мин
function setRandomMine(matrix){
    //берется рандомная клетка
    const cell=getRandomFreeCell(matrix);
    //устанавливается мина
    cell.mine=true;
    //число на клетках вокруг становится 1
    const cells=getAroundCells(matrix,cell.x,cell.y)
    for(const cell of cells){
        cell.number+=1;
    }
};

function getFlags(matrix){
    let arrFlags=[] 
   for (let y=0; y<matrix.length;y++){
        for (let x=0;x<matrix[y].length;x++){ 
            const cell=matrix[x][y]
            if (cell.flag ){
    arrFlags.push(cell)} 
    if(!cell.flag){
        arrFlags.pop(cell)
    }
            }
            return arrFlags.length
}

}

//функция для получения 1 клетки
function getCell(matrix,x,y){
    if(!matrix[y] || !matrix[y][x]){
        return false;
    } 
    return matrix[y][x];
};
//функция для получения клеток вокруг клетки с миной
function getAroundCells(matrix,x,y){
    //массив для этих клеток
    const cells=[];
    //пробегаюсь по клеткам вокруг
    for (let dx= -1;dx<=1;dx++){
        for (let dy= -1;dy<=1;dy++){
            if(dx === 0 && dy === 0){
                continue;
            } 
            //получаю клетку
            const cell=getCell(matrix,x+dx,y+dy);
            if (cell){
                //отправляю в массив 
                cells.push(cell)
            }

        }
    }
    return cells;
};
//получение клетки по айди
function getCellById(matrix,id){
    //пробегаюсь по матрице
    for (let y=0;y<matrix.length;y++){
        for (let x=0;x<matrix[y].length;x++){
            const cell=matrix[y][x];
            //если ее айди = заданому айдито возращаю эту клетку
            if (cell.id === id){
                return cell;
            }
        }
    }
    return false;
}

// DOM-дерево
function matrixToHtml(matrix){
    //создаю основной элемент для игры
    const gameElement=document.createElement('div');
    //добавляю ему класс
    gameElement.classList.add('sapper');
    gameElement.setAttribute("id","field")
    //для каждого рядка создается элемент
    for(let y=0;y<matrix.length;y++){
        const rowElement=document.createElement('div');
        rowElement.classList.add('row');
        //в каждом рядке по количеству columns (8) создается элемент-картинка
        for (let x=0;x<matrix[y].length;x++){
            const cell=matrix[y][x];
            const imgElement=document.createElement('img');
            //что б при нажатии и удержании картинки не двигались 
            //imgElement.draggable=false;
            //что бы при нажатии ЛКМ по картинки не вылазило контекстное меню
            imgElement.oncontextmenu=()=>false;
            //добавила аттрибут для каждой картинки
            imgElement.setAttribute('cell-id',cell.id);
            imgElement.setAttribute('id','cells')
            //добавляю как дочерний элемент для рядка
            rowElement.append(imgElement);
            //установка картинок,в зависимости что находится в клетке
            if(cell.flag){
                imgElement.src="flag.png";

                continue;
            }   
            if(cell.poten){
                imgElement.src="poten.png";
                continue;
            }  
            if(!cell.show){
                imgElement.src="free.png";
                continue;
            }  
            if(cell.mine){
                imgElement.src="red.png";
                continue;
            }  
            if(cell.number){
                
                imgElement.src="num"+cell.number+".png";
                continue;
            }
            if(cell.win){
                imgElement.src="win.png"
            }
            imgElement.src="none.png";
        }
        gameElement.append(rowElement);
        
    }
    return gameElement;

};

//обработчик для клетки
function forEach(matrix,handler){
    for (let y=0;y<matrix.length;y++){
        for (let x=0;x<matrix[y].length;x++){
            handler(matrix[x][y]);
        }
    }
};
//показ клеток без мин
function showSpread (matrix, x, y) {
    const cell = getCell(matrix, x, y);

    if (cell.flag || cell.number || cell.mine) {
        return;
    }

    forEach(matrix, x => x._marked = false);
    
    cell._marked = true;
    //forEach(matrix, x => delete x._marked );
    let flag = true;
    while (flag) {
        flag = false;

        for (let y = 0; y < matrix.length; y++) {
            for (let x = 0; x < matrix[y].length;x++){
                const cell = matrix[y][x];

                if (!cell._marked || cell.number) {
                    continue;
                }

                const cells = getAroundCells(matrix, x, y);
                for (const cell of cells) {
                    if (cell._marked) {
                        continue;
                    }

                    if (!cell.flag && !cell.mine) {
                        cell._marked = true;
                        flag = true;
                    }
                }
            }
        }
    }

    forEach(matrix, x => {
        if (x._marked) {
            x.show = true;
        }

        delete x._marked});
}

function counterForFlags(matrix){
    const flags = [];
    forEach(matrix, cell => {
        
        if (cell.flag) {
            flags.push(cell);
            
        }
 
})
    return flags.length
}
function counterForMines(matrix){
    const mines = [];
    forEach(matrix, cell => {
        
        if (cell.mine ) {
            mines.push(cell);
            
        }
 
})
    return mines.length
}


function gameOver(cell){
    forEach(matrix, cell => {
        if (cell.mine && !cell.show) {
            cell.show=true
        }
})
return cell}
function isWin (matrix) {
    
    const flags = [];
    const mines = [];
    
    forEach(matrix, cell => {
        if (cell.flag) {
            flags.push(cell);
        }

        if (cell.mine) {
            mines.push(cell);
        }
    })
    

    if (flags.length !== mines.length) {
        return false;
    }

    for (const cell of mines) {
        if (!cell.flag) {
            return false;
        }
    }

    for (let y = 0; y < matrix.length; y++) {
        for (let x = 0; x < matrix[y].length;x++){
            const cell = matrix[y][x];

            if(!cell.mine && !cell.show) {
                return false;
            }
        }
    }
    return true;
}
function showMines(matrix){
    forEach(matrix, cell => {
        if (cell.mine && !cell.show) {
            cell.show=true
        }
    })
    return cell
    
}
function isLoosing (matrix) {
    
    for (let y = 0; y < matrix.length; y++) {
        for (let x = 0; x < matrix[y].length;x++){

            const cell = matrix[y][x];
            if(cell.mine && cell.show) {
                forEach(matrix, cell => {
                    if (cell.mine && !cell.show) {
                        cell.show=true
                    }
                })
                return cell
                
                
            }}
          
        }
    }
