let matrix=null;
let running=null;
//создается матрица
init(8,8,8);
//создание кнопки для рестарта

//Перезапуск/запуск
function init (columns,rows,mines) {
    matrix=getMatrix(columns,rows);
    running=true;
    for (let i=0;i<10;i++){
        setRandomMine(matrix)
        update();
    };

}

//для обновления картинки
function update(){
    if(!running){
        return;
    };
    let showRes=document.querySelector('#count')
    let winOrLoose=document.querySelector('#end')
    const gameElement=matrixToHtml(matrix);
console.log(gameElement);
const appElement=document.querySelector('#app');
appElement.innerHTML="";
appElement.append(gameElement);
appElement.querySelectorAll('img')
.forEach(imgElement => {
    imgElement.addEventListener('mousedown',mousedownHandler);
    imgElement.addEventListener('mouseup',mouseupHandler);
    imgElement.addEventListener('mouseleave',mouseleaveHandler);

})
counterForMines(matrix)
counterForFlags(matrix)
showRes.innerHTML=`Расставлено флажков ${counterForFlags(matrix)}/${counterForMines(matrix)} Количество мин`
if(isLoosing(matrix)){
    winOrLoose.innerHTML='Вы проиграли :('
    showMines(matrix);
    running = false;

}
else if (isWin(matrix)){ 
    winOrLoose.innerHTML='Вы выиграли :)'
    running=false;
   

}
;}

function restartButton(){
    let but=document.createElement('button')
    let gf=document.querySelector('#second')
    gf.innerHTML="Для начала новой игры нажмите:"
    gf.append(but)
    but.setAttribute=('id','restart')
    but.innerHTML='Restart'
    but.addEventListener('click',()=>{
        init(8,8,8)});
    
    }
function deleteButton(){
    
    let gf=document.querySelector('#second')
    let but=document.querySelector('#restart')
        gf.remove(but); 
}


//функция если зажата кнопка мыши
function mousedownHandler(event){
    const {cell,left,right}=getInfo(event);
    if(left){
        cell.left=true;
        restartButton()
        
    }
    if(right){
        cell.right=true;  
    }
    if(cell.left && cell.right){
        bothHandler(cell);
    }
    update();
    

}
//если кнопка поднята
function mouseupHandler(event){
    const {left,right,cell}=getInfo(event);
    const both=cell.right && cell.left && (left || right);
    const leftMouse=!both && cell.left && left;
    const rightMouse=!both && cell.right && right;
    if(both){
        forEach(matrix, x => x.poten = false);
    }
  
    if(left){
        cell.left=false;

    }
    if(right){
        cell.right=false;
    }
    if(leftMouse){
        leftHandler(cell);
    }
    else if(rightMouse){
        rightHandler(cell);
    }
    update();
 }
 //отпущена
function mouseleaveHandler(event){
    const info=getInfo(event);
    info.cell.left=false;
    info.cell.right=false;
    update();
}

//для получения информации
function getInfo(event){
    const element=event.target;
    const cellId=parseInt(element.getAttribute('cell-id'));
    return{ 
        left:event.which === 1,
        right:event.which === 3,
        cell:getCellById(matrix,cellId)
    };
}
//леовя нажатие
function leftHandler(cell){
    if (cell.show || cell.flag){
        return;
    }
    cell.show=true;
        showSpread(matrix,cell.x,cell.y);
};
//правое
function rightHandler(cell){
    if (!cell.show){
        cell.flag=!cell.flag;
    }
};
//зажаты обе
function bothHandler (cell) {
    if (!cell.show || !cell.number) {
        return;
    }

    const cells = getAroundCells(matrix, cell.x, cell.y);
    const flags = cells.filter(x => x.flag).length;

    if (flags === cell.number) {
        cells
            .filter(x => !x.flag && !x.show)
            .forEach(cell => {
                cell.show = true
                showSpread(matrix, cell.x, cell.y)
            })
    }

    else {
        cells
            .filter(x => !x.flag && !x.show)
            .forEach(cell => cell.poten = true)
    }
}
